FROM golang:1.8
ENV GOPATH /go
RUN go get -u github.com/golang/dep/cmd/dep
WORKDIR /go/src/bitbucket.com/newsaggregator
COPY . .
RUN dep ensure -v -vendor-only
RUN go install -v ./...