//models
//Пакет реализуйщий методы и структуры для работы с базой

package models

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

//Config Конфигурация подключения к БД
type Config struct {
	Port     string
	Host     string
	User     string
	Password string
	Database string
}

//DTO Реализует интерфейс Datastore
type DTO struct {
	db  *sql.DB
	Cfg *Config
}

//ConnectTO подключение к БД
func (dto *DTO) connect() error {
	var err error
	dto.db, err = sql.Open("postgres",
		fmt.Sprintf("user=%s password=%s port=%s host=%s dbname=%s sslmode=disable", dto.Cfg.User, dto.Cfg.Password, dto.Cfg.Port, dto.Cfg.Host, dto.Cfg.Database))
	if err != nil {
		return err
	}
	if err = dto.db.Ping(); err != nil {
		return err
	}

	return nil
}

func (dto *DTO) close() {
	err := dto.db.Close()
	if err != nil {
		log.Println(err)
	}
}
