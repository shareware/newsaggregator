package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.com/newsaggregator/models"
	"github.com/PuerkitoBio/goquery"
)

var (
	config string
	app    *App
)

//App конфигурации приложения
type App struct {
	dto    Datastore
	config *Config
	cache  *Cache
}

//Config  конфигурация настроек источников данных
type Config struct {
	Sources []Source
}

//Source конфигурация источника данных
type Source struct {
	Type  string `json:"Type"`
	Name  string `json:"Name"`
	URL   string `json:"URL"`
	Parse struct {
		NewsContainer string `json:"NewsContainer"`
		Header        string `json:"Header"`
		Body          string `json:"Body"`
		ID            string `json:"ID"`
		URL           string `json:"URL"`
	}
}

//Datastore Описывает методы работы с базой
type Datastore interface {
	All() ([]*models.News, error)          //Позвращает все посты
	Add(*models.News) error                //Добавление поста
	Search(string) ([]*models.News, error) //Возвращает результат поиска
}

func init() {
	flag.StringVar(&config, "config", "", "set path to config")
}

func main() {
	flag.Parse()
	if config == "" {
		fmt.Println("args not set for help -h")
		return
	}
	db := &models.DTO{
		Cfg: &models.Config{
			Port:     os.Getenv("POSTGRES_PORT"),
			Host:     os.Getenv("POSTGRES_HOST"),
			User:     os.Getenv("POSTGRES_USER"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
			Database: os.Getenv("POSTGRES_DB"),
		},
	}
	app = &App{
		dto:    db,
		config: &Config{},
		cache:  NewCache(100),
	}
	app.ReadConfig(config)
	newsTimer := time.Minute * 10
	newsChan := GetNews(newsTimer)
	go WriteNews(newsChan)

	mux := http.NewServeMux()
	mux.HandleFunc("/news", mainHandler)
	mux.HandleFunc("/get", searchHandler)
	log.Printf("Server started at http://127.0.0.1:%s port.", os.Getenv("APP_PORT"))
	if err := http.ListenAndServe(":"+os.Getenv("APP_PORT"), mux); err != nil {
		panic("Can't run server: " + err.Error())
	}

}

//ReadConfig Десериализует конфиг файл
//path - Путь до файла конфига в формате .json
func (e *App) ReadConfig(path string) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(file, &e.config)
	if err != nil {
		panic(err)
	}
}

//WriteNews Запись новых новостей в базу.
//c <- канал из которого приходят новости
//Для уменьшения количества обращений к базе имеется проверка на наличие id поста в кэше.
func WriteNews(c <-chan *models.News) {
	for {
		select {
		case n := <-c:
			if !app.cache.Exist(n.ID) {
				err := app.dto.Add(n)
				if err != nil {
					log.Println(err.Error())
					break
				}
				app.cache.Push(n.ID)
			}
		}
	}
}

//GetNews Возвращает канал из которого приходят структуры новостей
//timer - Задержка перед следующим запросом данных.
func GetNews(timer time.Duration) <-chan *models.News {
	newsChan := make(chan *models.News)
	go func() {
		for {
			select {
			default:
				for _, source := range app.config.Sources {
					go getNews(source, newsChan)
				}
				time.Sleep(timer)
			}
		}
	}()
	return newsChan
}

func rssFeeder(r *http.Response, c chan *models.News) {
	var rss models.RSS
	rss.Unmarshal(r.Body)
	for _, news := range rss.Channel.Items {
		n := &models.News{
			Header: template.HTML(news.Title),
			Body:   template.HTML(news.Description),
			URL:    news.Link,
			ID:     news.GUID,
		}
		c <- n
	}

}

func htmlFeeder(r *http.Response, source Source, c chan *models.News) {
	doc, err := goquery.NewDocumentFromReader(r.Body)
	if err != nil {
		log.Println(err.Error())
		return
	}
	var exist bool
	var url, id string

	doc.Find(source.Parse.NewsContainer).Each(func(i int, s *goquery.Selection) {
		id, exist = s.Attr("id")
		if !exist {
			return
		}
		url, exist = s.Find(source.Parse.URL).Attr("href")
		if !exist {
			return
		}
		news := &models.News{
			Header: template.HTML(s.Find(source.Parse.Header).Text()),
			Body:   template.HTML(s.Find(source.Parse.Body).Text()),
			URL:    url,
			ID:     id,
		}
		c <- news
	})
}

//getNews Запрашивает последние посты из source.URL.
//Передает результат парсинга по правилам из source.Parse в канал.
func getNews(source Source, c chan *models.News) {
	r, err := http.Get(source.URL)
	if err != nil {
		log.Println(err.Error())
	}
	if source.Type == "html" {
		htmlFeeder(r, source, c)
	}
	if source.Type == "rss" {
		rssFeeder(r, c)
	}

}

//mainHandler Хэндрер страницы /news
func mainHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./templates/main.gohtml")
	if err != nil {
		log.Println(err.Error())
		return
	}
	n, err := app.dto.All()
	if err != nil {
		log.Println(err.Error())
		return
	}

	if err := tmpl.Execute(w, n); err != nil {
		log.Println(err)
	}
}

//searchHandler Хэндрер страницы /get
func searchHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./templates/main.gohtml")
	if err != nil {
		log.Println(err)
		return
	}

	keys, ok := r.URL.Query()["search"]

	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param 'search' is missing")
		return
	}
	key := keys[0]

	n, err := app.dto.Search(key)
	if err != nil {
		log.Println(err.Error())
		return
	}
	if err := tmpl.Execute(w, n); err != nil {
		log.Println(err)
	}
}
