package models

import (
	"fmt"
	"html/template"
	"log"
	"strings"
)

//News структура новости
type News struct {
	Header template.HTML
	Body   template.HTML
	URL    string
	ID     string
}

const (
	insertNews = "INSERT INTO news (id,header,body,url) VALUES ($1,$2,$3,$4)"
	selectNews = "SELECT * FROM news"
	searchNews = "SELECT * FROM news WHERE LOWER(header) LIKE '%%%s%%'"
)

//Add Добавить новость
func (dto *DTO) Add(n *News) error {
	err := dto.connect()
	if err != nil {
		return err
	}
	defer dto.close()
	_, err = dto.db.Exec(insertNews, n.ID, n.Header, n.Body, n.URL)
	if err != nil {
		return err
	}
	return nil
}

//All Возвращает все новости из базы
func (dto *DTO) All() ([]*News, error) {
	err := dto.connect()
	if err != nil {
		return nil, err
	}
	defer dto.close()
	return dto.selectQ(selectNews)
}

//Search Поиск в базе по header
func (dto *DTO) Search(s string) ([]*News, error) {
	err := dto.connect()
	if err != nil {
		return nil, err
	}
	defer dto.close()
	s = fmt.Sprintf(searchNews, strings.ToLower(s))
	return dto.selectQ(s)
}

func (dto *DTO) selectQ(s string) ([]*News, error) {
	rows, err := dto.db.Query(s)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	news := make([]*News, 0)

	for rows.Next() {
		n := new(News)
		err := rows.Scan(&n.ID, &n.Header, &n.Body, &n.URL)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		news = append(news, n)
	}
	return news, nil
}
