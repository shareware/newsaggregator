#News Aggregator

Сервер агрегации новостей написаный на Go.
Данные запрашиваются по ссылкам c интервалом в 10 минут:

1. На сайт
2. На RSS канал

Данные сохраняются в базу Postgres.
Поиск осуществляется по заголовку.

##Запуск
Для запуска вам потребуется установленный Docker и Git.

1. Клонируйте репозиторий в свое окружение
`git clone https://shareware@bitbucket.org/shareware/newsaggregator.git`
2. Сборка
    `docker-compose build`
3. Запуск
    `docker-compose up`
4. Вход на главную страницу достпен по ссылке [News Aggregator](http://localhost:8080/news)
5. Для остановки приложения выполните
    `docker-compose down`
    
##Конфигурация
Для настройки источников данных воспользуйтесь файлом Config.json
###Структура файла Config.json
    {
        "Sources":[
            {
                "Type":"rss",
                "Name":"TProger",
                "URL":"https://habr.com/ru/rss/all/all/?fl=ru"
            },
            {
                "Type":"html",
                "Name":"TProger",
                "URL":"https://tproger.ru/news/",
                "Parse":{
                    "NewsContainer":"article",
                    "Header":".entry-title",
                    "Body":".entry-content",
                    "ID":"article",
                    "URL":"a"
                }
            }
        ]
    }

* **Блок Sources** - Список источников и параметры парсинга
* **Type** - Тип источника запроса `rss` или `html`
* **URL** - Ссылка на новостной источник
* **Блок Parse** - Настройки парсинга для типа истоника html
  Примечание: Тип `rss` обрабатывается согласно спецификации [RSS v2.0](http://www.rssboard.org/rss-specification), при типе `rss` блок Parse не обязателен.
  Допустимые значения ключей:
    1. `.Class`
    2. `#ID`
    3. `Tag`
* **NewsContainer** - Родительский тег для каждого поста
* **Header** - Тег содержимым которого является заголовок поста
* **Body** - Тег содержимым которого является тело поста
* **ID** - Тег в атрибутах которого имеется уникальный идентификатор поста
* **URL** - Тег в аргументах которого имеется ссылка на пост
  Примечание: Значение берется из атрибута `href`

Для конфигурации приложения воспользуйтесь файлом `docker-compose.yml`



