package main

import (
	"sync"
)

//Cache Потокобезопасный кэш в виде очереди в котором хранятся идентификаторы постов.
type Cache struct {
	sync.RWMutex
	deep int
	c    []string
}

//NewCache Создает кэш с глубиной хранения deep
func NewCache(deep int) *Cache {
	return &Cache{
		deep: deep,
		c:    make([]string, 0, deep),
	}
}

//Push Добавить индентификатор в конец
func (cache *Cache) Push(id string) {
	if len(cache.c) >= cache.deep {
		cache.Pop()
	}
	cache.Lock()
	defer cache.Unlock()
	cache.c = append(cache.c, id)
}

//Exist Возвращает true если id имеется в кэше
func (cache *Cache) Exist(id string) (exist bool) {
	cache.RLock()
	defer cache.RUnlock()
	for _, i := range cache.c {
		if i == id {
			return true
		}
	}
	return false
}

//Pop Удалить первый элемент
func (cache *Cache) Pop() {
	cache.Lock()
	defer cache.Unlock()
	cache.c = cache.c[1:]
}

//Len Возвращает количество элементов
func (cache *Cache) Len() int {
	cache.RLock()
	defer cache.RUnlock()
	return len(cache.c)
}
