package models

import (
	"encoding/xml"
	"io"
	"io/ioutil"
	"log"
)

//RSS Описывает структуру RSS канала согласно спецификации RSS 2.0 http://www.rssboard.org/rss-specification
type RSS struct {
	XName   xml.Name `xml:"rss"`
	Channel struct {
		Items []struct {
			Title       string `xml:"title"`
			Link        string `xml:"link"`
			Description string `xml:"description"`
			GUID        string `xml:"guid"`
		} `xml:"item"`
	} `xml:"channel"`
}

//Unmarshal Инициализирует структуру RSS  из переданного буфера
func (rss *RSS) Unmarshal(r io.Reader) {
	buf, err := ioutil.ReadAll(r)

	if err != nil {
		log.Println(err.Error())
		return
	}

	err = xml.Unmarshal(buf, rss)
	if err != nil {
		log.Println(err.Error())
		return
	}

}
